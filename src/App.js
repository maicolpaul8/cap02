//import logo from './logo.svg';
import './App.css';
import styled from 'styled-components';

import Card from './Card';
import Submenu from './Submenu';

const Container=styled.div`

`;

const App=()=>{
  return (<Container>
    <Card></Card>
    <Submenu></Submenu>
  </Container>);
}

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

export default App;
