import React from "react";

import  styled  from "styled-components";

import Options from './options';

const Container=styled.div`
    background-color:#21d0d0;
    border-radius:25px;
    padding:15px;   
    height:150px;
    width:400px;    
    padding-top:15px; 
`

const App=()=>{

    const options=[
        {
            title:"PRICE LOW TO HIGH",
            state:false
        },
        {
            title:"PRICE HIGH TO LOW",
            state:false
        },
        {
            title:"popularity",
            state:true
        }                         

    ];

    return (
        <Container>
            {
                options.map((v,i)=>
                   <Options key={i} title={v.title}>

                    </Options>
                )
            }            
        </Container>
    );

}

export default App;