import React ,{ useState,useEffect }from "react";

import  styled  from "styled-components";

const Container=styled.div`
    margin-left:25px;
    margin-top:15px;
    display:flex;
    flex:direction:row;
    justify-content:space-between;
    margin-right:25px;
`
const Title=styled.p`
    margin:0px;
    color:white;
    font-weight:500;

`
const Circulo=styled.div`
    border-radius:50%;
    border:1.8px solid white;
    height:35px;
    width:35px;
    cursor:pointer;
    background-color:${props=>props.visible ? 'white' : 'transparent'}
`

const App=({title,state})=>{

    const [visible,setVisible]=useState(false);

    useEffect(()=>{
        // if(state){
        //     setVisible(true);
        // }else{
        //     setVisible(false);
        // }
        setVisible(state);
    },[state]);

    const handleChange=()=>{
        setVisible(!visible);
    }

    return (
        <Container>
            <Title>{title}</Title>
            <Circulo visible={visible} onClick={handleChange}></Circulo>
        </Container>
    );

}

export default App;